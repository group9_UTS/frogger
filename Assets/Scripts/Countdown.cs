﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
    public Text timerText;
    public float startTime = 100f;
    public float seconds, minutes;
    SoundManager soundManager;
    

    void Start()
    {
        //timerText = GetComponent<Text>() as Text;
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        //StartCoroutine(Observer15(startTime-15));
    }

    // Update is called once per frame
    void Update()
    {
        startTime -= 1 * Time.deltaTime;
        minutes = (int)(startTime / 60f);
        seconds = (int)(startTime % 60f);
       // print(startTime);
        timerText.text = "Time - " + minutes.ToString("00") + ":" + seconds.ToString("00");

        if(startTime <= 0)
        {
            startTime = 0;
        }
    }
    /*
    IEnumerator Observer15(float timetowait) {
        yield return new WaitForSeconds(timetowait);
        soundManager.playSound(SoundManager.Sound.TimeRunning);
    }
    */
}
