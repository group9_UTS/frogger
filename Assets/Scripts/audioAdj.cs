﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class audioAdj : MonoBehaviour
{
    //public AudioMixer mixer;
    private Slider slider;

    void Start() {
        slider = gameObject.GetComponent<Slider>();
        slider.value = PlayerPrefs.GetFloat("volume");
            }
    private void Update()
    {
        float sliderValue = slider.value;
        PlayerPrefs.SetFloat("volume", sliderValue);
    }
}
