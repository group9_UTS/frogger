﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Animator animator;
    private bool isHopping;
    float zDifference = 0;
    public int lives = 3;
    public Text score;

    bool fellInWater;
    bool canMove;

    float hopTimer;

    public GameObject victoryScreen;
    public TextMeshProUGUI winText;


    [SerializeField] private Transform player;
    [SerializeField] private Transform respawnPoint;
    SoundManager soundManager;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        //animator = GetComponent<Animator>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isHopping)
        {
            hopTimer += Time.deltaTime;
            if (hopTimer  > 0.2f)
            {
                finishHop();
            }
            
        }

        if(transform.position == respawnPoint.transform.position)
        {
            fellInWater = false;
        }

        if (canMove)
        {
            if (Input.GetKeyDown(KeyCode.W) && !isHopping)
            {
                animator.SetTrigger("hop");
                isHopping = true;

                if (transform.position.z % 1 != 0)
                {
                    zDifference = Mathf.Round(transform.position.z) - transform.position.z;
                }
                moveCharacter(new Vector3(1, 0, zDifference));
                transform.eulerAngles = new Vector3(0, 90, 0);
            }
            else if (Input.GetKeyDown(KeyCode.A) && !isHopping)
            {
                moveCharacter(new Vector3(0, 0, 1));

                if (transform.parent == null)
                    transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else if (Input.GetKeyDown(KeyCode.D) && !isHopping)
            {
                moveCharacter(new Vector3(0, 0, -1));

                if (transform.parent == null)
                    transform.eulerAngles = new Vector3(0, 180, 0);
            }
            else if (Input.GetKeyDown(KeyCode.S) && !isHopping)
            {
                if (transform.position.x % 1 != 0)
                {
                    zDifference = Mathf.Round(transform.position.x) - transform.position.x;
                }
                moveCharacter(new Vector3(-1, 0, zDifference));

                if (transform.parent == null)
                    transform.eulerAngles = new Vector3(0, -90, 0);
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.GetComponent<Log>() != null)
        {
            transform.parent = collision.collider.transform;
        }
        else
        {
            transform.parent = null;
        }

        if (collision.collider.tag == "Water")
        {
            if (!fellInWater)
            {
                fellInWater = true;
                player.transform.position = respawnPoint.transform.position;
                soundManager.playSound(SoundManager.Sound.Drown);
                Lifes.playerLives -= 1;
                if (Lifes.playerLives == 0)
                {
                    Die();
                }
            }
        }
    }
    
    private void Die()
    {
        //play sound
        soundManager.playSound(SoundManager.Sound.GameOver);
        canMove = false;
        StartCoroutine(LoseCondition());
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Vehicle")
        {
            //player respawn at starting point when lose a life instead of staying on the road
            player.transform.position = respawnPoint.transform.position;

            soundManager.playSound(SoundManager.Sound.Crash);
            Lifes.playerLives -= 1;
            if (Lifes.playerLives == 0)
            {
                Die();
            }

        }
        if (other.tag == "Goal")
        {
            
            canMove = false;
            soundManager.playSound(SoundManager.Sound.Victory);
            //win animation

            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level02"))
            {
                Victory();
            }
            StartCoroutine(WinCondition());
        }
    }

    IEnumerator WinCondition()
    {
        if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Level02"))
        {
            yield return new WaitForSeconds(7);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            if (SceneManager.GetActiveScene().buildIndex == 2)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
                Scene scene = SceneManager.GetActiveScene();
                if (scene.name == "Level01")
                {
                    SceneManager.LoadScene("Level02");
                }
            }
        }
    }

    
    IEnumerator LoseCondition()
    {
        float waitCount = 0f;
        Time.timeScale = 0;
        while (waitCount < 3.0f)
        {
            waitCount += Time.unscaledDeltaTime;

            yield return 0;
        }
        Time.timeScale = 1;
        lives = 3;
        SceneManager.LoadScene("GameOver");
    }
    
    private void moveCharacter(Vector3 difference)
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Level02"))
        {
            soundManager.playSound(SoundManager.Sound.Splash);
        }
        animator.SetTrigger("hop");
        isHopping = true;
        transform.position = transform.position + difference;
        soundManager.playSound(SoundManager.Sound.Jump);
    }

    public void finishHop()
    {
        isHopping = false;
    }

    public void Victory() 
    {
        Time.timeScale = 0f;
        victoryScreen.SetActive(true);
        winText = GameObject.FindGameObjectWithTag("WinText").GetComponent<TextMeshProUGUI>();
        winText.text = "YOU WON! \n YOU BEAT THE GAME WITH " + Lifes.playerLives.ToString() + " LIVES TO SPARE!";
    }
}
