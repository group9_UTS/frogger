﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Lifes : MonoBehaviour
{

    public static int playerLives = 3;
    public Text score;

    SoundManager soundManager;
    void Start()
    {
        playerLives = 3;
        score.text = playerLives.ToString();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }


    void Update()
    {
        score.text = "X " + playerLives;
       
    }
}




