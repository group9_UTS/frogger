﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public int maxRoadsCount;
    public List<GameObject> roads1 = new List<GameObject>();
    public List<GameObject> roads2 = new List<GameObject>();

    [SerializeField] private List<GameObject> currentRoads = new List<GameObject>();
    private Vector3 currentPos1 = new Vector3(2, 0, 0);
    private Vector3 currentPos2 = new Vector3(8, 0, 0);

    private void Start()
    {
        for (int i = 0; i < maxRoadsCount; i++)
        {
            SpawnRoad1();
        }
        for (int i = 0; i < maxRoadsCount; i++)
        {
            SpawnRoad2();
        }
    }

    private void SpawnRoad1()
    {
        GameObject road1 = Instantiate(roads1[Random.Range(0, roads1.Count)], currentPos1, Quaternion.identity);
        currentRoads.Add(road1);
        currentPos1.x++;
    }

    private void SpawnRoad2()
    {
        GameObject road2 = Instantiate(roads2[Random.Range(0, roads2.Count)], currentPos2, Quaternion.identity);
        currentRoads.Add(road2);
        currentPos2.x++;
    }
}
